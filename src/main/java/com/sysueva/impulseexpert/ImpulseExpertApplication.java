package com.sysueva.impulseexpert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImpulseExpertApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImpulseExpertApplication.class, args);
    }

}
