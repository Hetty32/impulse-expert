package com.sysueva.impulseexpert.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String name;
    private String secondName;
    private int grade;
    private String groupField;
}
