package com.sysueva.impulseexpert.controllers;

import com.sysueva.impulseexpert.entity.Student;
import com.sysueva.impulseexpert.repositories.StudentRepo;
import org.apache.logging.log4j.util.PropertySource;
import org.aspectj.weaver.Iterators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
public class MainController {

    @Autowired
    StudentRepo studentRepo;

    @GetMapping("/")
    public String main(@RequestParam(name = "orderBy", defaultValue = " ") String orderBy, Model model) {
        model.addAttribute("students", sortBy(orderBy));
        model.addAttribute("orderBy", orderBy);

        return "main";
    }

    private List<Student> sortBy(String orderBy) {
        List<Student> all =  StreamSupport
                .stream(studentRepo.findAll().spliterator(),false)
                .collect(Collectors.toList());
        switch (orderBy){
            case "firstName":
                all.sort(Comparator.comparing(Student::getFirstName));
                break;
            case "name":
                all.sort(Comparator.comparing(Student::getName));
                break;
            case "secondName":
                all.sort(Comparator.comparing(Student::getSecondName));
                break;
            case "grade":
                all.sort(Comparator.comparingInt(Student::getGrade));
                break;
            case "groupField":
                all.sort(Comparator.comparing(Student::getGroupField));
                break;
        }
        return all;
    }

    @GetMapping("/add")
    public String add(Model model) {
        model.addAttribute("student", new Student());
        return "add";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("student", studentRepo.findById(id));
        return "add";
    }


    @PostMapping("/add")
    public String save(@ModelAttribute Student student, Model model) {
        studentRepo.save(student);
        return "redirect:/";//если тут не сработает надо будет через @requestParam Добавлять поля и делать new  в методе
    }


    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id) {
        studentRepo.deleteById(id);
        return "redirect:/";
    }

}
