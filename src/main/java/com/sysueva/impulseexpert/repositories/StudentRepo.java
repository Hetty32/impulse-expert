package com.sysueva.impulseexpert.repositories;

import com.sysueva.impulseexpert.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentRepo extends JpaRepository<Student, Long> {
}
